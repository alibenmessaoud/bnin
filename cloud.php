<?php include("connect.php"); ?>
<?php
function tag_info() { 
  $result = mysql_query("SELECT * FROM post where title <> '' GROUP BY title ORDER BY view DESC limit 9"); 
  while($row = mysql_fetch_array($result)) { 
  
  
  		 $title = $row['title'] ;
	if (strlen($title) > 14) {  
	$title = substr($title, 0,14);}
  
  
  	
    $arr[$title] = $row['view'];
	  
  } 
  ksort($arr); 
  return $arr; 
}

function tag_cloud() {

    $min_size = 10;
    $max_size = 20;

    $tags = tag_info();

    $minimum_count = min(array_values($tags));
    $maximum_count = max(array_values($tags));
    $spread = $maximum_count - $minimum_count;

    if($spread == 0) {
        $spread = 1;
    }

    $cloud_html = '';
    $cloud_tags = array(); // create an array to hold tag code
    foreach ($tags as $tag => $count) {
        $size = $min_size + ($count - $minimum_count) 
            * ($max_size - $min_size) / $spread;
        $cloud_tags[] = '<a style="font-size: '. floor($size) . 'px' 
            . '" class="tag_cloud" >' 
            . htmlspecialchars(stripslashes($tag)) . '</a>';
    }
    $cloud_html = join("\n", $cloud_tags) . "\n";
    return $cloud_html;

}

?>

<style type="text/css">
.tag_cloud
	{padding: 3px; text-decoration: none;
	font-family: verdana; 	}
.tag_cloud:link  { color:#03F; }
.tag_cloud:visited { color: #9900FF; }
.tag_cloud:hover { color: #03F; background: #000000; }
.tag_cloud:active { color: #6699FF; background: #000000; }
</style>

<div id="wrapper">
<?php print tag_cloud(); ?></div>