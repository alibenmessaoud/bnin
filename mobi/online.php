<?php




/* Helper functions: */

function get_tag($tag,$xml)
{
	preg_match_all('/<'.$tag.'>(.*)<\/'.$tag.'>$/imU',$xml,$match);
	return $match[1];
}

function is_bot()
{
	/* This function will check whether the visitor is a search engine robot */
	
	$botlist = array("Teoma", "alexa", "froogle", "Gigabot", "inktomi",
	"looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory",
	"Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot",
	"crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp",
	"msnbot", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz",
	"Baiduspider", "Feedfetcher-Google", "TechnoratiSnoop", "Rankivabot",
	"Mediapartners-Google", "Sogou web spider", "WebAlta Crawler","TweetmemeBot",
	"Butterfly","Twitturls","Me.dium","Twiceler");

	foreach($botlist as $bot)
	{
		if(strpos($_SERVER['HTTP_USER_AGENT'],$bot)!==false)
		return true;	// Is a bot
	}

	return false;	// Not a bot
}










// We don't want web bots scewing our stats:
if(is_bot()) die();

//echo $stringIp = $_SERVER['REMOTE_ADDR'];
$stringIp = $_SERVER['REMOTE_ADDR'];
//$stringIp = $_GET['ip'];

// This user is not in the database, so we must fetch
	// the geoip data and insert it into the online table:
	
		// Making an API call to Hostip:
		
		$xml = file_get_contents('http://api.hostip.info/?ip='.$stringIp);
		
		$city = get_tag('gml:name',$xml);
		$city = $city[1];
		
		$countryName = get_tag('countryName',$xml);
		$countryName = $countryName[0];
		
		$countryAbbrev = get_tag('countryAbbrev',$xml);
		$countryAbbrev = $countryAbbrev[0];
		
		// Setting a cookie with the data, which is set to expire in a month:
	//	setcookie('geoData',$city.'|'.$countryName.'|'.$countryAbbrev, time()+60*60*24*30,'/');


	$countryName = str_replace('(Unknown Country?)','UNKNOWN',$countryName);
	
	// In case the Hostip API fails:
		
	if (!$countryName)
	{
		$countryName='UNKNOWN';
		$countryAbbrev='xx';
		$city='(Unknown City?)';
	}
	
/* echo "<br> $countryName <br>" ;
echo "<br> $city <br>" ;
echo "<br> $countryAbbrev <br>" ;
echo "<br>  <br>" ;

 */


?> 
 